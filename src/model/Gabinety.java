package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Gabinety database table.
 * 
 */
@Entity
@NamedQuery(name="Gabinety.findAll", query="SELECT g FROM Gabinety g")
public class Gabinety implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idGabinety;

	private int numerPokoju;

	//bi-directional many-to-one association to Lekarze
	@ManyToOne
	private Lekarze lekarze;

	//bi-directional many-to-one association to Kliniki
	@ManyToOne
	private Kliniki kliniki;

	public Gabinety() {
	}

	public int getIdGabinety() {
		return this.idGabinety;
	}

	public void setIdGabinety(int idGabinety) {
		this.idGabinety = idGabinety;
	}

	public int getNumerPokoju() {
		return this.numerPokoju;
	}

	public void setNumerPokoju(int numerPokoju) {
		this.numerPokoju = numerPokoju;
	}

	public Lekarze getLekarze() {
		return this.lekarze;
	}

	public void setLekarze(Lekarze lekarze) {
		this.lekarze = lekarze;
	}

	public Kliniki getKliniki() {
		return this.kliniki;
	}

	public void setKliniki(Kliniki kliniki) {
		this.kliniki = kliniki;
	}

}