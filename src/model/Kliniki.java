package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Kliniki database table.
 * 
 */
@Entity
@NamedQuery(name="Kliniki.findAll", query="SELECT k FROM Kliniki k")
public class Kliniki implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idKliniki;

	private String ardes;

	private String nazwa;

	//bi-directional many-to-one association to Gabinety
	@OneToMany(mappedBy="kliniki")
	private List<Gabinety> gabineties;

	public Kliniki() {
	}

	public int getIdKliniki() {
		return this.idKliniki;
	}

	public void setIdKliniki(int idKliniki) {
		this.idKliniki = idKliniki;
	}

	public String getArdes() {
		return this.ardes;
	}

	public void setArdes(String ardes) {
		this.ardes = ardes;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List<Gabinety> getGabineties() {
		return this.gabineties;
	}

	public void setGabineties(List<Gabinety> gabineties) {
		this.gabineties = gabineties;
	}

	public Gabinety addGabinety(Gabinety gabinety) {
		getGabineties().add(gabinety);
		gabinety.setKliniki(this);

		return gabinety;
	}

	public Gabinety removeGabinety(Gabinety gabinety) {
		getGabineties().remove(gabinety);
		gabinety.setKliniki(null);

		return gabinety;
	}

}