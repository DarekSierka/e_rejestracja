package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Apteki database table.
 * 
 */
@Entity
@NamedQuery(name="Apteki.findAll", query="SELECT a FROM Apteki a")
public class Apteki implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idApteki;

	private String nazwaApteki;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Users_idUsers")
	private User user;

	public Apteki() {
	}

	public int getIdApteki() {
		return this.idApteki;
	}

	public void setIdApteki(int idApteki) {
		this.idApteki = idApteki;
	}

	public String getNazwaApteki() {
		return this.nazwaApteki;
	}

	public void setNazwaApteki(String nazwaApteki) {
		this.nazwaApteki = nazwaApteki;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}