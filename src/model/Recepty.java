package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Recepty database table.
 * 
 */
@Entity
@NamedQuery(name="Recepty.findAll", query="SELECT r FROM Recepty r")
public class Recepty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idRecepty;

	private int czyZnizka;

	private String peselPacjenta;

	private String przypisaneLeki;

	private int wielkoscZnizki;

	private int zrealizowana;

	//bi-directional many-to-one association to Wizyty
	@OneToMany(mappedBy="recepty")
	private List<Wizyty> wizyties;

	public Recepty() {
	}

	public int getIdRecepty() {
		return this.idRecepty;
	}

	public void setIdRecepty(int idRecepty) {
		this.idRecepty = idRecepty;
	}

	public int getCzyZnizka() {
		return this.czyZnizka;
	}

	public void setCzyZnizka(int czyZnizka) {
		this.czyZnizka = czyZnizka;
	}

	public String getPeselPacjenta() {
		return this.peselPacjenta;
	}

	public void setPeselPacjenta(String peselPacjenta) {
		this.peselPacjenta = peselPacjenta;
	}

	public String getPrzypisaneLeki() {
		return this.przypisaneLeki;
	}

	public void setPrzypisaneLeki(String przypisaneLeki) {
		this.przypisaneLeki = przypisaneLeki;
	}

	public int getWielkoscZnizki() {
		return this.wielkoscZnizki;
	}

	public void setWielkoscZnizki(int wielkoscZnizki) {
		this.wielkoscZnizki = wielkoscZnizki;
	}

	public int getZrealizowana() {
		return this.zrealizowana;
	}

	public void setZrealizowana(int zrealizowana) {
		this.zrealizowana = zrealizowana;
	}

	public List<Wizyty> getWizyties() {
		return this.wizyties;
	}

	public void setWizyties(List<Wizyty> wizyties) {
		this.wizyties = wizyties;
	}

	public Wizyty addWizyty(Wizyty wizyty) {
		getWizyties().add(wizyty);
		wizyty.setRecepty(this);

		return wizyty;
	}

	public Wizyty removeWizyty(Wizyty wizyty) {
		getWizyties().remove(wizyty);
		wizyty.setRecepty(null);

		return wizyty;
	}

}