package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Users database table.
 * 
 */
@Entity
@Table(name="Users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idUsers;

	private String haslo;

	private String imie;

	private String nazwisko;

	private String nick;

	private String pesel;

	//bi-directional many-to-one association to Apteki
	@OneToMany(mappedBy="user")
	private List<Apteki> aptekis;

	//bi-directional many-to-one association to Kartoteka
	@OneToMany(mappedBy="user")
	private List<Kartoteka> kartotekas;

	//bi-directional many-to-one association to Lekarze
	@OneToMany(mappedBy="user")
	private List<Lekarze> lekarzes;

	//bi-directional many-to-one association to Wizyty
	@OneToMany(mappedBy="user")
	private List<Wizyty> wizyties;

	public User() {
	}

	public int getIdUsers() {
		return this.idUsers;
	}

	public void setIdUsers(int idUsers) {
		this.idUsers = idUsers;
	}

	public String getHaslo() {
		return this.haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public String getImie() {
		return this.imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return this.nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPesel() {
		return this.pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public List<Apteki> getAptekis() {
		return this.aptekis;
	}

	public void setAptekis(List<Apteki> aptekis) {
		this.aptekis = aptekis;
	}

	public Apteki addApteki(Apteki apteki) {
		getAptekis().add(apteki);
		apteki.setUser(this);

		return apteki;
	}

	public Apteki removeApteki(Apteki apteki) {
		getAptekis().remove(apteki);
		apteki.setUser(null);

		return apteki;
	}

	public List<Kartoteka> getKartotekas() {
		return this.kartotekas;
	}

	public void setKartotekas(List<Kartoteka> kartotekas) {
		this.kartotekas = kartotekas;
	}

	public Kartoteka addKartoteka(Kartoteka kartoteka) {
		getKartotekas().add(kartoteka);
		kartoteka.setUser(this);

		return kartoteka;
	}

	public Kartoteka removeKartoteka(Kartoteka kartoteka) {
		getKartotekas().remove(kartoteka);
		kartoteka.setUser(null);

		return kartoteka;
	}

	public List<Lekarze> getLekarzes() {
		return this.lekarzes;
	}

	public void setLekarzes(List<Lekarze> lekarzes) {
		this.lekarzes = lekarzes;
	}

	public Lekarze addLekarze(Lekarze lekarze) {
		getLekarzes().add(lekarze);
		lekarze.setUser(this);

		return lekarze;
	}

	public Lekarze removeLekarze(Lekarze lekarze) {
		getLekarzes().remove(lekarze);
		lekarze.setUser(null);

		return lekarze;
	}

	public List<Wizyty> getWizyties() {
		return this.wizyties;
	}

	public void setWizyties(List<Wizyty> wizyties) {
		this.wizyties = wizyties;
	}

	public Wizyty addWizyty(Wizyty wizyty) {
		getWizyties().add(wizyty);
		wizyty.setUser(this);

		return wizyty;
	}

	public Wizyty removeWizyty(Wizyty wizyty) {
		getWizyties().remove(wizyty);
		wizyty.setUser(null);

		return wizyty;
	}

}