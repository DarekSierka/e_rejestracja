package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Lekarze database table.
 * 
 */
@Entity
@NamedQuery(name="Lekarze.findAll", query="SELECT l FROM Lekarze l")
public class Lekarze implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idLekarza;

	private String tytul;

	//bi-directional many-to-one association to Gabinety
	@OneToMany(mappedBy="lekarze")
	private List<Gabinety> gabineties;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Users_idUsers")
	private User user;

	//bi-directional many-to-one association to Wizyty
	@OneToMany(mappedBy="lekarze")
	private List<Wizyty> wizyties;

	public Lekarze() {
	}

	public int getIdLekarza() {
		return this.idLekarza;
	}

	public void setIdLekarza(int idLekarza) {
		this.idLekarza = idLekarza;
	}

	public String getTytul() {
		return this.tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public List<Gabinety> getGabineties() {
		return this.gabineties;
	}

	public void setGabineties(List<Gabinety> gabineties) {
		this.gabineties = gabineties;
	}

	public Gabinety addGabinety(Gabinety gabinety) {
		getGabineties().add(gabinety);
		gabinety.setLekarze(this);

		return gabinety;
	}

	public Gabinety removeGabinety(Gabinety gabinety) {
		getGabineties().remove(gabinety);
		gabinety.setLekarze(null);

		return gabinety;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Wizyty> getWizyties() {
		return this.wizyties;
	}

	public void setWizyties(List<Wizyty> wizyties) {
		this.wizyties = wizyties;
	}

	public Wizyty addWizyty(Wizyty wizyty) {
		getWizyties().add(wizyty);
		wizyty.setLekarze(this);

		return wizyty;
	}

	public Wizyty removeWizyty(Wizyty wizyty) {
		getWizyties().remove(wizyty);
		wizyty.setLekarze(null);

		return wizyty;
	}

}