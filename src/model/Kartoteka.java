package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Kartoteka database table.
 * 
 */
@Entity
@NamedQuery(name="Kartoteka.findAll", query="SELECT k FROM Kartoteka k")
public class Kartoteka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idKartoteka;

	private String notatka;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Users_idUsers")
	private User user;

	public Kartoteka() {
	}

	public int getIdKartoteka() {
		return this.idKartoteka;
	}

	public void setIdKartoteka(int idKartoteka) {
		this.idKartoteka = idKartoteka;
	}

	public String getNotatka() {
		return this.notatka;
	}

	public void setNotatka(String notatka) {
		this.notatka = notatka;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}