package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the Wizyty database table.
 * 
 */
@Entity
@NamedQuery(name="Wizyty.findAll", query="SELECT w FROM Wizyty w")
public class Wizyty implements Serializable, Comparable<Wizyty> {
	private static final long serialVersionUID = 1L;

	@Id
	private int idWizyty;

	@Temporal(TemporalType.DATE)
	private Date dataWizyty;

	private String godzinaWizyty;

	private int numerGabinetu;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Users_idUsers")
	private User user;

	//bi-directional many-to-one association to Lekarze
	@ManyToOne
	private Lekarze lekarze;

	//bi-directional many-to-one association to Recepty
	@ManyToOne
	private Recepty recepty;

	public Wizyty() {
	}

	public int getIdWizyty() {
		return this.idWizyty;
	}

	public void setIdWizyty(int idWizyty) {
		this.idWizyty = idWizyty;
	}

	public Date getDataWizyty() {
		return this.dataWizyty;
	}

	public void setDataWizyty(Date dataWizyty) {
		this.dataWizyty = dataWizyty;
	}

	public String getGodzinaWizyty() {
		return this.godzinaWizyty;
	}

	public void setGodzinaWizyty(String godzinaWizyty) {
		this.godzinaWizyty = godzinaWizyty;
	}

	public int getNumerGabinetu() {
		return this.numerGabinetu;
	}

	public void setNumerGabinetu(int numerGabinetu) {
		this.numerGabinetu = numerGabinetu;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Lekarze getLekarze() {
		return this.lekarze;
	}

	public void setLekarze(Lekarze lekarze) {
		this.lekarze = lekarze;
	}

	public Recepty getRecepty() {
		return this.recepty;
	}

	public void setRecepty(Recepty recepty) {
		this.recepty = recepty;
	}
	
	 @Override
	  public int compareTo(Wizyty o) {
		String g1 = getGodzinaWizyty();
		String g2 = o.getGodzinaWizyty();
		
		if(g1.charAt(0)>49)
			g1="0"+g1;
		if(g2.charAt(0)>49)
			g2="0"+g2;
		 
	    return g1.compareTo(g2);
	  }

}