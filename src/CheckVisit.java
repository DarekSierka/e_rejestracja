import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import model.User;
import model.Wizyty;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CheckVisit extends JFrame {

	private JPanel contentPane;
	private User loguser;

	/**
	 * Create the frame.
	 */
	public CheckVisit(User u) {
		loguser=u;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNewButton = new JButton("Close");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(40)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 407, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnNewButton)
					.addContainerGap(120, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(46, Short.MAX_VALUE))
		);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1));
		setVisits(panel);
		contentPane.setLayout(gl_contentPane);
		setVisible(true);
	}
	public void setVisits(JPanel panel){
		if(loguser.getWizyties().size()==0){
			JOptionPane.showMessageDialog(null,"No visit here");
			dispose();
		}
		List<Wizyty> wizyty = loguser.getWizyties();
		wizyty.sort(new Comparator<Wizyty>() {
		    @Override
		    public int compare(Wizyty o1, Wizyty o2) {
		        return o1.getDataWizyty().compareTo(o2.getDataWizyty());
		    }
		});
		System.out.println(wizyty.size());
		for(Wizyty w:wizyty){
			JLabel l = new JLabel();
			Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
			l.setBorder(border);
			l.setText(w.getDataWizyty().toString() + " o godzinie "+w.getGodzinaWizyty() +" w gabinecie "+String.valueOf(w.getNumerGabinetu())+"\nprzyjmnie Ci� dr "+w.getLekarze().getUser().getNazwisko());
			panel.add(l);
		}
	}
}
