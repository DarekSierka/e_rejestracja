import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Lekarze;
import model.MysqlTransaction;
import model.User;
import model.Wizyty;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseMotionAdapter;

public class NewVisit extends JFrame {

	private JPanel contentPane;

	private User User;
	private JComboBox comboBox_1;
	private JComboBox comboBox;
	private JDateChooser dateChooser;
	
	public NewVisit(String lekarz,User u,String godz,Date data) {
		User=u;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		dateChooser = new JDateChooser(new Date());
		dateChooser.setMinSelectableDate(new Date());
		
		comboBox = new JComboBox();
		setdoctors(comboBox);
		
		if(lekarz!=null){
			comboBox.setSelectedItem(lekarz);
		}
		
		comboBox_1 = new JComboBox();
		comboBox_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setHours(comboBox_1);
			}
		});
		
		if(data!=null){
			dateChooser.setDate(data);
		}		
		
		setHours(comboBox_1);
				
		if(godz!=null){
			System.out.println(godz);
			comboBox_1.setSelectedItem(godz);
		}
		
		JButton btnNewButton = new JButton("Booking");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addVisit();
			}
		});
		
		JButton btnNewButton_1 = new JButton("Close");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(56)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(348, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(562, Short.MAX_VALUE)
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addComponent(btnNewButton)
					.addPreferredGap(ComponentPlacement.RELATED, 158, Short.MAX_VALUE)
					.addComponent(btnNewButton_1)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void setHours(JComboBox box){
		System.out.println("Ustawiam godziny");		
		box.removeAll();
		ArrayList<String> lista = new ArrayList<String>();
		for(int i=0;i<32;i++)
			if(dateChooser.getDate().after(new Date())||((new Date().getHours()<8+i/4 || new Date().getHours()==8+i/4 && new Date().getMinutes()<i%4*15)))
				if(i%4==0)
					lista.add(8+i/4+":"+i%4*15+"0");
				else
					lista.add(8+i/4+":"+i%4*15);
		box.setModel(new DefaultComboBoxModel(lista.toArray()));
	}
	
	public void setdoctors(JComboBox box){
		box.removeAll();
		MysqlTransaction transaction = new MysqlTransaction();
		
		ArrayList<Lekarze> lekarze= (ArrayList<Lekarze>)transaction.getSession().createCriteria(Lekarze.class).list();
				
		transaction.finalizeSession();
		ArrayList<String> imional = new ArrayList<String>();
		for(Lekarze l:lekarze)
			imional.add(l.getTytul()+" "+l.getUser().getImie()+" "+l.getUser().getNazwisko());
		box.setModel(new DefaultComboBoxModel(imional.toArray()));
	}
	
	public void addVisit(){
		MysqlTransaction transaction = new MysqlTransaction();
		
		ArrayList<Lekarze> lekarze= (ArrayList<Lekarze>)transaction.getSession().createCriteria(Lekarze.class).list();
		
		Wizyty visit = new Wizyty();
		visit.setUser(User);
		
		List<Wizyty> wizyty =null;		
		
		for(Lekarze l:lekarze)
			if(String.valueOf(comboBox.getSelectedItem()).equals(l.getTytul()+" "+l.getUser().getImie()+" "+l.getUser().getNazwisko())){
				wizyty = l.getWizyties();
				visit.setLekarze(l);
				visit.setNumerGabinetu(l.getGabineties().get(0).getNumerPokoju());
				break;
			}
		if(wizyty==null){
			JOptionPane.showMessageDialog(null,"This doctors is not in our database");
			setdoctors(comboBox);
			transaction.finalizeSession();
			return;
		}
		String godzina = String.valueOf(new Date().getHours()) + String.valueOf(new Date().getMinutes());
		
		if(!dateChooser.getDate().after(new Date())&&comboBox_1.getSelectedItem().toString().compareTo(godzina)<1){
			JOptionPane.showMessageDialog(null,"Time left");
			setHours(comboBox_1);
			transaction.finalizeSession();
			return;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(dateChooser.getDate()).toString();
		for(Wizyty w:wizyty){
			if(w.getDataWizyty().toString().equals(date))
				if(w.getGodzinaWizyty().equals(comboBox_1.getSelectedItem().toString())){
					JOptionPane.showMessageDialog(null,"There is visit in time that You want to book");
					transaction.finalizeSession();
					return;
				}
		}
		
		visit.setGodzinaWizyty(comboBox_1.getSelectedItem().toString());
		visit.setDataWizyty(dateChooser.getDate());
		
		transaction.save(visit);
		
		transaction.finalizeSession();
		
		JOptionPane.showMessageDialog(null,"Your visit is added");
	}
}
