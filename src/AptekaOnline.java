import java.awt.Button;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import KlasyPomocnicze.MessageClass;
import KlasyPomocnicze.ValidationUserClass;
import model.Apteki;
import model.MysqlTransaction;
import model.Recepty;
import model.User;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class AptekaOnline extends JFrame {

	private JPanel contentPane;
	private User user;
	private Apteki apteka;
	private JTextField textField;
	private JPanel panel;
	private ArrayList<Recepty> recepty = new ArrayList<Recepty>();
	private JScrollPane scrollPane;
	
	/**
	 * Create the frame.
	 */
	public AptekaOnline(User u) {
		MysqlTransaction transaction = new MysqlTransaction();
		recepty= (ArrayList<Recepty>)transaction.getSession().createCriteria(Recepty.class).list();
		transaction.finalizeSession();
		
		user = u;
		apteka = u.getAptekis().get(0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Apteka: "+apteka.getNazwaApteki());
		
		JLabel lblNewLabel_1 = new JLabel("User name: "+user.getImie());
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Find prescription");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPrescription();
			}
		});
		
		JLabel lblNewLabel_2 = new JLabel("Pesel:");
		
		JButton btnNewButton_1 = new JButton("Logout");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new OknoLogowania();
				dispose();
			}
		});
		
		scrollPane = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnNewButton_1, Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_1))
							.addPreferredGap(ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE)
							.addGap(44)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel_2)
									.addGap(20)
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(btnNewButton, Alignment.TRAILING))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_2)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_1)
								.addComponent(btnNewButton))
							.addPreferredGap(ComponentPlacement.RELATED, 253, Short.MAX_VALUE)
							.addComponent(btnNewButton_1)))
					.addContainerGap())
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(32, 2));
		contentPane.setLayout(gl_contentPane);
		setVisible(true);
		setLocationRelativeTo(null);
	}
	
	public void showPrescription(){
		
		panel.removeAll();
		
		if(!ValidationUserClass.isNumeric(textField.getText())||!(textField.getText().length()==11)){
			new MessageClass("Pesel is incorect");
			return;
		}		
		
		for(Recepty r :recepty)
			if(r.getZrealizowana()==0 &&r.getPeselPacjenta().equals(textField.getText())){
				if(r.getCzyZnizka()==0)
					panel.add(new JLabel(r.getPrzypisaneLeki()));
				else
					panel.add(new JLabel(r.getPrzypisaneLeki()+" zni�ka: "+r.getWielkoscZnizki()+"%"));
				Button but = new Button("finalize");
				but.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Realizuj(r);
						recepty.remove(r);
						new MessageClass("Finalize");
						showPrescription();
					}
				});
				panel.add(but);
			}

		scrollPane.setViewportView(panel);

	}
	
	public void Realizuj(Recepty r){
		MysqlTransaction transaction = new MysqlTransaction();
		
		ArrayList<Recepty> recepty= (ArrayList<Recepty>)transaction.getSession().createCriteria(Recepty.class).list();
		for(Recepty re :recepty)
			if(re.getIdRecepty()==r.getIdRecepty()){
				re.setZrealizowana(1);
				break;
			}
		
		transaction.finalizeSession();
	}
}
