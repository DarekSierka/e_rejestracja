import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import org.hibernate.HibernateException;

import com.mysql.jdbc.ResultSet;

import KlasyPomocnicze.Init;
import KlasyPomocnicze.MessageClass;
import KlasyPomocnicze.StringConverter;
import model.MysqlTransaction;
import model.User;

import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.security.Key;
import java.sql.SQLException;

public class OknoLogowania extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField textField_1;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try{
					OknoLogowania frame = new OknoLogowania();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OknoLogowania() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("Create account");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewP();
			}
		});
		
		JButton btnNewButton_1 = new JButton("Log in");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logging();
			}
		});
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyCode()==10)
					logging();
			}
		});
		textField.setColumns(10);
		
		textField_1 = new JPasswordField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyCode()==10)
					logging();
			}
		});
		textField_1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("User Name :");
		
		lblNewLabel_1 = new JLabel("Password :");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(156)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
						.addComponent(textField_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
						.addComponent(btnNewButton_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
					.addGap(160))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(104)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
						.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addGap(34)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addGap(107))
		);
		contentPane.setLayout(gl_contentPane);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void logging(){
		String nazwa=textField.getText();
		String haslo=textField_1.getText();		
		
		if(StringConverter.isNull(nazwa)||StringConverter.isNull(haslo))
			return;
		
		haslo = StringConverter.PasswordCode(haslo); 
		MessageClass.messageOnConsole(nazwa+"\n"+haslo);
		User logUser = getUser(nazwa, haslo);
		
		if(logUser==null)			
			return;
		
		logHim(logUser);
		
	}
	
	public void logHim(User logUser){
		
		if(logUser.getLekarzes().size()==0&&logUser.getAptekis().size()==0)
			new Online(this,logUser);
		else 
			if(logUser.getLekarzes().size()!=0)
				new DoctorsOnline(this,logUser.getLekarzes().get(0));
			else
				new AptekaOnline(logUser);
	}
	
	public User getUser(String nazwa,String haslo){
		User logUser = null;
		int id=-1;
		
		String polecenie = "SELECT idUsers FROM User WHERE nick LIKE '"+nazwa+"' and haslo LIKE '"+haslo+"'";		
		MysqlTransaction transaction = new MysqlTransaction();
		try{
			id = (int) transaction.getSession().createQuery(polecenie).uniqueResult();
		}catch(NullPointerException e){
			new MessageClass("B��dne has�o lub nazwa u�ytkownika");
			return null;
		}
		logUser = (User) transaction.getSession().get(User.class, id);
		
		transaction.finalizeSession();
		
		if(logUser==null){
			new MessageClass("B��dne has�o lub nazwa u�ytkownika");
			return null;
		}
		return logUser;
	}
}
