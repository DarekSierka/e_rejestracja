import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.soap.Text;

import org.hibernate.dialect.MySQL5Dialect;

import KlasyPomocnicze.MessageClass;
import model.Kartoteka;
import model.Lekarze;
import model.MysqlTransaction;
import model.Recepty;
import model.User;
import model.Wizyty;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JCheckBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;

public class DuringVisit extends JFrame {

	private JPanel contentPane;
	private User pacjent;
	private Lekarze lekarz;
	
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTextPane textPane;
	private JTextPane textPane_1;
	private JCheckBox chckbxNewCheckBox;
	
	private int numberOfCure = 0;
	private JTextField textField;
	/**
	 * Create the frame.
	 */
	public DuringVisit(User pac, Lekarze leka,Wizyty wizyta) {
		pacjent = pac;
		lekarz  = leka;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 466);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("Close");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		JButton btnNewButton_1 = new JButton("Add cure");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCure();
			}
		});
		
		JButton btnNewButton_2 = new JButton("Remove recent cure");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeRecentCure();
			}
		});
		
		scrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		JScrollPane scrollPane_2 = new JScrollPane();
		
		JButton btnNewButton_3 = new JButton("Add note");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNote();
			}
		});
		
		JButton btnNewButton_5 = new JButton("All notes");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AllNotes(pacjent);
			}
		});
		
		JLabel lblNewLabel = new JLabel("New cure");
		
		JLabel lblNewLabel_1 = new JLabel("Prescription");
		
		JButton btnNewButton_4 = new JButton("End Visit");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				endVisit();
			}
		});
		
		JLabel lblNewLabel_2 = new JLabel("Patient: "+pacjent.getImie()+" "+pacjent.getNazwisko());
		
		JLabel lblNewLabel_3 = new JLabel("Pesel: "+pacjent.getPesel());
		
		chckbxNewCheckBox = new JCheckBox("Discount");
		chckbxNewCheckBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(chckbxNewCheckBox.isSelected())
					textField.setEditable(true);
				else
					textField.setEditable(false);
			}
		});
		
		textField = new JTextField();		
		textField.setColumns(10);
		textField.setEditable(false);
		
		JLabel lblNewLabel_4 = new JLabel("%");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(btnNewButton_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnNewButton_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE))
								.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_1)))
						.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 432, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel_4)
							.addGap(115))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_3)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(btnNewButton_5, GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
										.addComponent(btnNewButton_3, GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
										.addComponent(btnNewButton_4, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)))
								.addComponent(lblNewLabel_2)
								.addComponent(chckbxNewCheckBox))
							.addContainerGap())))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(16)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton_1)
							.addGap(18)
							.addComponent(btnNewButton_2))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNewLabel_2)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblNewLabel_3)
							.addGap(18)
							.addComponent(chckbxNewCheckBox)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_4)))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton_3)
								.addComponent(btnNewButton_4))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton)
								.addComponent(btnNewButton_5)))
						.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		
		textPane_1 = new JTextPane();
		scrollPane_2.setViewportView(textPane_1);
		
		textPane = new JTextPane();
		scrollPane_1.setViewportView(textPane);
		
		panel = new JPanel();
		scrollPane.setColumnHeaderView(panel);		
		panel.setLayout(new GridLayout(32, 1));
		contentPane.setLayout(gl_contentPane);
		setVisible(true);
		setLocationRelativeTo(null);
	}
		
	public void endVisit(){
		
		int discount=0;
		if(chckbxNewCheckBox.isSelected())
			if(isNumeric(textField.getText())){
				discount = Integer.parseInt(textField.getText());
				if( discount <= 0 || discount >= 100){
					new MessageClass("Less then 100 more the 0");
					return;
				}
				
			}else{
				new MessageClass("Discount is not numeric value");
				return;
			}
				
		Component[] con = panel.getComponents();
		
		String leki="";
		for(Component c:con){
			JLabel label = (JLabel)c;
			leki = leki + label.getText() +"\n";
		}
		
		Recepty recepta = new Recepty();
		if (chckbxNewCheckBox.isSelected()) 
			recepta.setCzyZnizka((byte)1); 
		else
			recepta.setCzyZnizka((byte)0);
		recepta.setPeselPacjenta(pacjent.getPesel());
		recepta.setWielkoscZnizki(discount);
		recepta.setPrzypisaneLeki(leki);
		recepta.setZrealizowana((byte)0);
		
		MysqlTransaction transaction = new MysqlTransaction();
		
		transaction.save(recepta);
		transaction.finalizeSession();
		new MessageClass("Visit was ended");
		dispose();
	}
	
	public void addNote(){
		if(textPane_1.getText() == null || textPane_1.getText().length() < 3){
			new MessageClass("Minimum 3 characters");
			return;
		}
		
		Kartoteka kartoteka = new Kartoteka();
		kartoteka.setUser(pacjent);
		kartoteka.setNotatka(textPane_1.getText());
				
		MysqlTransaction transaction = new MysqlTransaction();
		transaction.save(kartoteka);
		
		transaction.finalizeSession();
		
		pacjent.getKartotekas().add(kartoteka);
		new MessageClass("Your note was added");
	}
	
	public void addCure(){
	
		if(textPane.getText() == null || textPane.getText().length() < 3){
			new MessageClass("Minimum 3 characters");
			return;
		}
		
		numberOfCure++;
		panel.add(new JLabel(numberOfCure+" "+textPane.getText()));
		scrollPane.setViewportView(panel);
		textPane.setText("");
	}
	
	public void removeRecentCure(){
		
		if(numberOfCure==0){
			new MessageClass("There is no cures to delete");
			return;
		}
		numberOfCure--;
		panel.remove(numberOfCure);
		scrollPane.setViewportView(panel);
	}
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Integer.parseInt(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
}
