import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Lekarze;
import model.Wizyty;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import com.toedter.calendar.JDateChooser;

import KlasyPomocnicze.MessageClass;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;

public class VisitCalendar extends JFrame {

	private JPanel contentPane;
	private Lekarze lekarz;
	private JDateChooser dateChooser;
	private JPanel panel;
	private JScrollPane scrollPane;
	
	/**
	 * Create the frame.
	 */
	public VisitCalendar(Lekarze le) {
		this.lekarz=le;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		scrollPane = new JScrollPane();
		
		JButton btnNewButton = new JButton("Close");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		JButton btnNewButton_1 = new JButton("Show Visits");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showVisits();
			}
		});
		
		dateChooser = new JDateChooser(new Date());
		dateChooser.setMinSelectableDate(new Date());		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(29)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnNewButton_1)
						.addComponent(dateChooser, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton_1)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnNewButton))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);		
		panel.setLayout(new GridLayout(32, 2));
		contentPane.setLayout(gl_contentPane);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public void showVisits(){
		panel.removeAll();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Wizyty> wizytki = (List<Wizyty>) lekarz.getWizyties();
		Collections.sort(wizytki);
		for(Wizyty w : wizytki)
			if(sdf.format(dateChooser.getDate()).compareTo(sdf.format(w.getDataWizyty()))==0){
								
				panel.add(new JLabel(sdf.format(w.getDataWizyty()) +" "+w.getGodzinaWizyty()));
				JButton button = new JButton(w.getUser().getImie()+" "+w.getUser().getNazwisko());
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						new DuringVisit(w.getUser(),lekarz,w);
						dispose();
					}
				});
				panel.add(button);
			}
		scrollPane.setViewportView(panel);
	}
}
