import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import KlasyPomocnicze.MessageClass;
import KlasyPomocnicze.VisitExtractor;
import model.MysqlTransaction;
import model.User;
import model.Wizyty;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;

public class Online extends JFrame {

	private JPanel contentPane;

	private User loguser;
	
	/**
	 * Create the frame.
	 */
	public Online(JFrame o,User logU) {
		o.dispose();
		this.loguser=logU;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Online.class.getResource("/pictures/pies_lekarz (1).jpg")));
		
		JLabel lblNewLabel_1 = new JLabel(logU.getImie());
		
		JLabel lblNewLabel_2 = new JLabel(logU.getNazwisko());
		
		JLabel lblNewLabel_3 = new JLabel(logU.getPesel());
		
		JButton btnNewButton = new JButton("Exit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		JButton btnNewButton_1 = new JButton("New visit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewVisit(null,loguser,null,null);
			}
		});
		
		JButton btnNewButton_2 = new JButton("Check doctors");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CheckDoctor(loguser);
			}
		});
		
		JButton btnNewButton_3 = new JButton("Check visit");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CheckVisit(loguser);
			}
		});
		
		JButton btnNewButton_4 = new JButton("Check the nearest visit");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nearestvisit();
			}
		});
		
		JButton btnNewButton_5 = new JButton("Logout");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new OknoLogowania();
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton_3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblNewLabel_3, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
								.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
								.addComponent(lblNewLabel_2, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
								.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
							.addGap(111))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton_2, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
								.addComponent(btnNewButton_4, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE))
							.addGap(111)))
					.addGap(183)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(btnNewButton_5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
					.addGap(20))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addComponent(lblNewLabel_1)
					.addGap(18)
					.addComponent(lblNewLabel_2)
					.addGap(18)
					.addComponent(lblNewLabel_3)
					.addGap(66)
					.addComponent(btnNewButton_1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addComponent(btnNewButton_4))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(30)
							.addComponent(btnNewButton_5)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_2)
						.addComponent(btnNewButton))
					.addContainerGap(49, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel, 0, 0, Short.MAX_VALUE)
					.addGap(122))
		);
		contentPane.setLayout(gl_contentPane);
		
		setLocationRelativeTo(null);
		setVisible(true);
		
		new Thread(){
			public void run(){
				
				MessageClass.messageOnConsole("Tu jestem");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				MysqlTransaction transaction = new MysqlTransaction();				
				
				for(Wizyty w: loguser.getWizyties() )
					if(sdf.format(w.getDataWizyty()).compareTo(sdf.format(new Date())) < 0)
						transaction.getSession().delete(w);
				
				transaction.finalizeSession();
				MessageClass.messageOnConsole("Tu jestem2");
			}
			
		}.start();
		
	}
	
	public void nearestvisit(){
		if(loguser.getWizyties().size()==0){
			new MessageClass("No visit here");
		}else{
			
			String wizyta = VisitExtractor.ExtractNearestVisit(loguser.getWizyties().get(loguser.getWizyties().size()-1));
			
			if(wizyta!=null)
				new MessageClass(wizyta);
		}
			
	}
	
}
