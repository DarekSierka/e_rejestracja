import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import KlasyPomocnicze.MessageClass;
import KlasyPomocnicze.StringConverter;
import KlasyPomocnicze.ValidationUserClass;
import model.MysqlTransaction;
import model.User;

public class NewP  {

	
	public JFrame newp;
	
	
	private JLabel imielabel= new JLabel("First Name");
	private JLabel nazwiskolabel = new JLabel("Last Name");
	private JLabel nicklabel = new JLabel("Nick");
	private JLabel haslolabel = new JLabel("Password");
	private JLabel pesellabel = new JLabel("Pesel");

	private JTextField imietext = new JTextField();
	private JTextField nazwiskotext = new JTextField();
	private JTextField nicktext = new JTextField();
	private JTextField haslotext = new JTextField(); 
	private JTextField peseltext = new JTextField();
	
	private JButton create = new JButton("Create");
	private JButton cancel = new JButton("Close");
	
	
	private int sizeX=700;
	private int sizeY=400;
	
	
	public NewP(){
		//loggingpanel.setEnabled(false);
		newp=new JFrame();		
		createpanel();
	}
	
	public void createpanel(){
		setElements();
		setLiseners();
	}
	
	public void setLiseners(){
		create.addActionListener(new Lisener());
		cancel.addActionListener(new Lisener());
	}
	
	public void setElements(){
		
		newp.setTitle("Okno tworzenia konta");
		newp.setSize(sizeX, sizeY);
		
		int X=50;
		int Y=50;
		int width=70;
		int height=30;
		
		imielabel.setBounds(X, Y, width, height);
		nazwiskolabel.setBounds(X, Y+height +2, width, height);
		nicklabel.setBounds(X, Y+2*height +2, width, height);
		haslolabel.setBounds(X, Y+3*height +2, width, height);
		pesellabel.setBounds(X, Y+4*height +2, width, height);
		
		newp.add(imielabel);
		newp.add(nazwiskolabel);
		newp.add(nicklabel);
		newp.add(haslolabel);
		newp.add(pesellabel);
		
		X=X+width;
		width=2*width;
		imietext.setBounds(X, Y, width, height);
		nazwiskotext.setBounds(X, Y+height +2, width, height);
		nicktext.setBounds(X, Y+2*height +2, width, height);
		haslotext.setBounds(X, Y+3*height +2, width, height);
		peseltext.setBounds(X, Y+4*height +2, width, height);
		
		newp.add(imietext);
		newp.add(nazwiskotext);
		newp.add(nicktext);
		newp.add(haslotext);
		newp.add(peseltext);
		
		create.setBounds(X, Y+5*height +2, width, height);
		cancel.setBounds(X+width, Y+5*height +2, width, height);
		
		newp.add(create);
		newp.add(cancel);
		
		JLabel o = new JLabel();
		o.setVisible(false);
		newp.add(o);
		
		
		newp.setLocationRelativeTo(null);
		newp.setVisible(true);
	}
	
	public void creating(){	
		
		String imie=imietext.getText();
		String nazwisko=nazwiskotext.getText();
		String nick=nicktext.getText();
		String haslo=haslotext.getText();
		String pesel=peseltext.getText();
		
		if( ValidationUserClass.userValidation(imie, nazwisko, pesel, nick, haslo) )
			return;
		
		
		if( addUser(imie, nazwisko, pesel, nick, haslo) )
			return;
		
		newp.dispose();
	}	
	
	public boolean addUser(String imie,String nazwisko, String pesel ,String nick, String haslo){
		
		MysqlTransaction transaction = new MysqlTransaction();
		
		if( !ValidationUserClass.uniqueUser(transaction,nick,pesel))
			return true;
		
		haslo = StringConverter.PasswordCode(haslo);
		User nowy = new User();
		
		nowy.setHaslo(haslo);
		nowy.setImie(imie);
		nowy.setNazwisko(nazwisko);
		nowy.setNick(nick);
		nowy.setPesel(pesel);

		
		transaction.save(nowy);
		
		transaction.finalizeSession();
		new MessageClass("User is aded");
		
		return false;
	}
	
	class Lisener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
	      	
	      	if(e.getActionCommand().equals("Create")){
	      		creating();
	      	}
	      	if(e.getActionCommand().equals("Close")){
	      		newp.dispose();
	      	}
	      	
		}
		
	}
	
}
