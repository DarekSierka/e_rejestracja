import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JCalendar;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import com.toedter.calendar.JDateChooser;

import KlasyPomocnicze.MessageClass;
import model.Lekarze;
import model.MysqlTransaction;
import model.User;
import model.Wizyty;

import javax.swing.Box;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CheckDoctor extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JDateChooser dateChooser;
	private JComboBox comboBox;
	private User loguser;
	/**
	 * @wbp.nonvisual location=295,109
	 */
	private final Component verticalStrut = Box.createVerticalStrut(20);
	
	/**
	 * Create the frame.
	 */
	public CheckDoctor(User u) {
		loguser=u;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 698, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		comboBox = new JComboBox();
		
		JButton btnNewButton = new JButton("Check");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addtopanel(panel);
			}
		});
		
		dateChooser = new JDateChooser();
		dateChooser.setMinSelectableDate(new Date());
		
		scrollPane = new JScrollPane();
		
		JButton btnNewButton_1 = new JButton("Close");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
							.addGap(89)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 336, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 303, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
							.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnNewButton)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
							.addComponent(btnNewButton_1)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(32, 2));
		contentPane.setLayout(gl_contentPane);
		setLocationRelativeTo(null);
		setVisible(true);
		
		
		setdoctors(comboBox);
		setdata(dateChooser);
	}
	
	public void setdata(JDateChooser dateChooser){
		
		dateChooser.setDate(new Date());
	}
	
	public void setdoctors(JComboBox box){
		MysqlTransaction transaction = new MysqlTransaction();
		
		ArrayList<Lekarze> lekarze= (ArrayList<Lekarze>)transaction.getSession().createCriteria(Lekarze.class).list();
		ArrayList<String> imional = new ArrayList<String>();
		
		for(Lekarze l:lekarze)
			imional.add(l.getTytul()+" "+l.getUser().getImie()+" "+l.getUser().getNazwisko());
		box.setModel(new DefaultComboBoxModel(imional.toArray()));
				
		transaction.finalizeSession();
	}
	
	public void addtopanel(JPanel panel){
		panel.removeAll();
		MysqlTransaction transaction = new MysqlTransaction();
		
		ArrayList<Lekarze> lekarze= (ArrayList<Lekarze>)transaction.getSession().createCriteria(Lekarze.class).list();
		
		transaction.finalizeSession();
		List<Wizyty> wizyty =null;
		
		for(Lekarze l:lekarze)
			if(String.valueOf(comboBox.getSelectedItem()).equals(l.getTytul()+" "+l.getUser().getImie()+" "+l.getUser().getNazwisko())){
				wizyty = l.getWizyties();
				break;
			}
		
		ArrayList<Wizyty> okrojonewizyty = new ArrayList<Wizyty>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for(Wizyty w:wizyty){
			if(String.valueOf(w.getDataWizyty()).equals(sdf.format(dateChooser.getDate())))
				okrojonewizyty.add(w);
		}
				
		for(int i=0;i<32;i++){
			String pelnegodziny = 8+i/4+":"+i%4*15+"0";
			String innegodziny = 8+i/4+":"+i%4*15;
			
			if(i%4==0)
				panel.add(new JLabel(pelnegodziny,SwingConstants.CENTER));
			else
				panel.add(new JLabel(innegodziny,SwingConstants.CENTER));
			
			
			if(sdf.format(dateChooser.getDate()).compareTo(sdf.format(new Date()))<=0&&(new Date().getHours()>8+i/4 || new Date().getHours()==8+i/4 &&new Date().getMinutes()>i%4*15)){
				JButton but = new JButton("Disable");
				but.setBackground(new Color(128,128,128));
				but.disable();
				panel.add(but);
				continue;
			}
			
			if(okrojonewizyty.size()==0){
				JButton but = new JButton("Free");
				but.setBackground(new Color(0,102,0));
				but.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(innegodziny.length()==innegodziny.indexOf(":")+2)
							new NewVisit(comboBox.getSelectedItem().toString(),loguser,pelnegodziny,dateChooser.getDate());
						else
							new NewVisit(comboBox.getSelectedItem().toString(),loguser,innegodziny,dateChooser.getDate());
					}
				});
				panel.add(but);
			}else{
				int addBookedButton=0;
				for(Wizyty w :okrojonewizyty)
					if(w.getGodzinaWizyty().equals(8+i/4+":"+i%4*15+"0")||w.getGodzinaWizyty().equals(8+i/4+":"+i%4*15)){
						JButton but = new JButton("Booked");
						but.setBackground(new Color(204,0,0));
						panel.add(but);
						addBookedButton = 1;
						break;
					}
				if(addBookedButton == 0){
					JButton but = new JButton("Free");
					but.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							if(innegodziny.length()==innegodziny.indexOf(":")+2)
								new NewVisit(comboBox.getSelectedItem().toString(),loguser,pelnegodziny,dateChooser.getDate());
							else
								new NewVisit(comboBox.getSelectedItem().toString(),loguser,innegodziny,dateChooser.getDate());
						}
					});
					but.setBackground(new Color(0,102,0));
					panel.add(but);
				}
			}
			
		}
		scrollPane.setViewportView(panel);
		
	}
}
