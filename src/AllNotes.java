import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import KlasyPomocnicze.MessageClass;
import model.MysqlTransaction;
import model.User;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class AllNotes extends JFrame {

	private JPanel contentPane;
	private int numberOfNote  ;
	private User pacjent;
	private JTextPane textPane;
	
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnEdit;
	
	/**
	 * Create the frame.
	 */
	public AllNotes(User pac) {
		pacjent = pac;
		numberOfNote = pacjent.getKartotekas().size();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		btnNewButton = new JButton("previous");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				previousNote();
			}
		});
		
		btnNewButton_1 = new JButton("next");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextNote();
			}
		});
		
		btnNewButton_2 = new JButton("Delete");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteNote();
			}
		});
		
		btnEdit = new JButton("Save");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNote();
			}
		});
		
		JButton btnNewButton_3 = new JButton("Close");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(42)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(btnNewButton_2)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnEdit)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnNewButton)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
					.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(39, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton_3)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton_1)
								.addComponent(btnNewButton)
								.addComponent(btnNewButton_2)
								.addComponent(btnEdit))
							.addGap(18)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)))
					.addGap(31))
		);
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		contentPane.setLayout(gl_contentPane);
		setVisible(true);
		setLocationRelativeTo(null);
		
		
		setNotes();
	}
	
	
	public void updateNote(){
		
		MysqlTransaction transaction = new MysqlTransaction();
		
		pacjent.getKartotekas().get(numberOfNote - 1).setNotatka(textPane.getText());
		transaction.finalizeSession();
		
		new MessageClass("Note was updated");
	}
	
	public void setNotes(){
		textPane.setText("");
		
		if(pacjent.getKartotekas().size()==0){
			new MessageClass("There are no notes");
			setButtons();
			return;
		}
		setButtons();
		
		textPane.setText(pacjent.getKartotekas().get(numberOfNote-1).getNotatka());		
		
	}
	public void setButtons(){
		if(numberOfNote == 1||numberOfNote == 0)
			btnNewButton.setEnabled(false);
		else
			btnNewButton.setEnabled(true);
		
		if(numberOfNote == pacjent.getKartotekas().size())
			btnNewButton_1.setEnabled(false);
		else
			btnNewButton_1.setEnabled(true);
		
	}
	public void deleteNote(){
		
		MysqlTransaction transaction = new MysqlTransaction();
		
		transaction.getSession().delete(pacjent.getKartotekas().get(numberOfNote - 1));
		
		transaction.finalizeSession();
		
		pacjent.getKartotekas().remove(numberOfNote - 1);
		numberOfNote--;
		setNotes();
		
		new MessageClass("Note was deleted");
		
	}
	
	public void previousNote(){
		numberOfNote--;
		setNotes();
	}
	
	public void nextNote(){
		numberOfNote++;
		setNotes();
	}
}
