package KlasyPomocnicze;

import java.util.Date;

import javax.swing.JOptionPane;

import model.Wizyty;

public class VisitExtractor {
	
	public VisitExtractor(){
		
	}
	
	public static String ExtractNearestVisit(Wizyty wizyta){
		 
		if(wizyta==null)
			return null;
		
		if(wizyta.getDataWizyty().compareTo(new Date())<0){
			new MessageClass("No visit here");
			return null;
		}
		
		String data = wizyta.getDataWizyty() .toString();
		String godzina = wizyta.getGodzinaWizyty();
		String lekarz = wizyta.getLekarze().getUser().getNazwisko() ;
		String gabinet = String.valueOf(wizyta.getNumerGabinetu());
		
		return data+" o godzinie "+godzina+" w gabinecie nr: "+gabinet+" przyjmie Ci� dr. "+lekarz;
	}
}
