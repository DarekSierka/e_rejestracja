package KlasyPomocnicze;

public class StringConverter {

	public static String PasswordCode(String haslo){

		char [] h = haslo.toCharArray();
		
		for(int i=0;i<haslo.length();i++)
			for(int z=0;z<i%3;z++)
				h[i]++;
		
		return String.valueOf(h);
	}
	
	public static boolean isNull(String text){
		if(text.length()==0){
			new MessageClass("brak wymaganych danych!");
			return true;
		}
		
		return false;
	}
	
}
