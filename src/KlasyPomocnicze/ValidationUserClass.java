package KlasyPomocnicze;

import javax.swing.JOptionPane;

import model.MysqlTransaction;

public class ValidationUserClass {

	public ValidationUserClass(){
		
	}
	
	public static boolean userValidation(String imie,String nazwisko, String pesel ,String nick, String haslo){
		
		if(imie.length()==0||nazwisko.length()==0||nick.length()==0||haslo.length()==0||pesel.length()==0){
			
			new MessageClass("Brak niekt�rych danych");
			return true;
		}
		
		if (!imie.matches("[a-zA-Z]+$")||!nazwisko.matches("[a-zA-Z]+$")) {
			
			new MessageClass("Imie i nazwisko nie mog� zawiera� cyfr");
			return true;
		}
		
		if(!isNumeric(pesel)){
			new MessageClass("Pesel nie jest cyfr�");
			return true;
		}
		
		if(pesel.length()!=11){
			new MessageClass("Pesel nie ma 11 cyfr");
			return true;
		}
		
		return false;
	}	
	
	public static boolean uniqueUser(MysqlTransaction transaction, String nick , String pesel){
		long liczba=0;
		
		String polecenie = "Select Count(*) from User where nick like '"+nick+"'";
		liczba = (long) transaction.getSession().createQuery(polecenie).uniqueResult();

		if(liczba >0){
			new MessageClass("Istnieje osoba o podanym nicku");
			transaction.finalizeSession();
			return false;
		}
		
		liczba=0;
		polecenie = "Select Count(*) from User where pesel like '"+pesel+"'";
		liczba = (long) transaction.getSession().createQuery(polecenie).uniqueResult();
		
		if(liczba>0){
			new MessageClass("Istnieje osoba o podanym peselu");
			transaction.finalizeSession();
			return false;
		}
		return true;
	}
	
	public static boolean isNumeric(String str)
	  {
	    try
	    {
	      double d = Double.parseDouble(str);
	    }
	    catch(NumberFormatException nfe)
	    {
	      return false;
	    }
	    return true;
	  }
	
}
