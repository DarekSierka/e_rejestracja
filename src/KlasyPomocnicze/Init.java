package KlasyPomocnicze;
import java.util.ArrayList;

import model.Apteki;
import model.Gabinety;
import model.Kliniki;
import model.Lekarze;
import model.MysqlTransaction;
import model.Recepty;
import model.User;
import model.Wizyty;

public class Init {

	MysqlTransaction transaction;
	private ArrayList<Kliniki> kliniki = new ArrayList<Kliniki>();
	private ArrayList<Lekarze> lekarze = new ArrayList<Lekarze>();
	private ArrayList<Recepty> recepty = new ArrayList<Recepty>();
	private ArrayList<Wizyty> wizyty = new ArrayList<Wizyty>();
	private ArrayList<User> users = new ArrayList<User>();
	private ArrayList<Gabinety> gabinety = new ArrayList<Gabinety>();
	private ArrayList<Apteki> apteki = new ArrayList<Apteki>();
	
	public Init(){
		initDataBase();
	}
	
	public void initDataBase(){
		this.transaction = new MysqlTransaction();
		System.out.println("Inicjuj�");
		
		kliniki= (ArrayList<Kliniki>)transaction.getSession().createCriteria(Kliniki.class).list();
		lekarze= (ArrayList<Lekarze>)transaction.getSession().createCriteria(Lekarze.class).list();
		recepty= (ArrayList<Recepty>)transaction.getSession().createCriteria(Recepty.class).list();
		wizyty= (ArrayList<Wizyty>)transaction.getSession().createCriteria(Wizyty.class).list();
		users= (ArrayList<User>)transaction.getSession().createCriteria(User.class).list();
		gabinety= (ArrayList<Gabinety>)transaction.getSession().createCriteria(Gabinety.class).list();
		apteki= (ArrayList<Apteki>)transaction.getSession().createCriteria(Apteki.class).list();
		// pobieram tabele do list
		
		//addkliniki();		
		//addLekarze();
		//addgabinet();	
		//addUserApteki();
		//addApteki();
		//users.get(0).setHaslo("lfmas|135");
		//users.get(1).setHaslo("lfmas|135");
		
		this.transaction.finalizeSession();
		System.out.println("I'm done");
	}
	public void addApteki(){
		Apteki apteka =  new Apteki();
		apteka.setUser(users.get(3));
		apteka.setNazwaApteki("Magiczna Apteka");
		transaction.save(apteka);
	}
	public void addUserApteki(){
		User user = new User();
		user.setImie("Farmacuta1");
		user.setNazwisko("");
		user.setNick("apteka1");
		user.setHaslo("apteczka");
		user.setPesel("00000000001");
		user.setWizyties(null);
		transaction.save(user);
	}
	
	public void addkliniki(){
		Kliniki klinika = new Kliniki();
		klinika.setArdes(" Opalenica ul. Bukowska 9 ");
		klinika.setNazwa(" Szpital Sw. Anny ");
		kliniki.add(klinika);
		
		transaction.save(klinika);
	}
	public void addLekarze(){
		Lekarze lekarz = new Lekarze();
		User user = new User();
		
		user.setImie("Krzysztof2");
		user.setNazwisko("Jarzyna2");
		user.setNick("doktor2");
		user.setHaslo("lfmas|135");
		user.setPesel("11122233444");
		user.setWizyties(null);
		
		transaction.save(user);
		
		this.transaction.finalizeSession();
		
		this.transaction = new MysqlTransaction();
		
		lekarz.setUser(users.get(1));
		lekarz.setTytul("prof. dr.");
		transaction.save(lekarz);
		
	}
	
	public void addgabinet(){
		Gabinety g = new Gabinety();
		g.setKliniki(kliniki.get(0));
		g.setNumerPokoju(69);
		g.setLekarze(lekarze.get(1));
		transaction.save(g);
		
	}
}
